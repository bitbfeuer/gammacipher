﻿namespace BogushZaoLab
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxGenA = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxGenX0 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxGenC = new System.Windows.Forms.TextBox();
            this.groupBoxGenerator = new System.Windows.Forms.GroupBox();
            this.textBoxOpentext = new System.Windows.Forms.TextBox();
            this.textBoxCiphertext = new System.Windows.Forms.TextBox();
            this.groupBoxOpentext = new System.Windows.Forms.GroupBox();
            this.groupBoxciphertext = new System.Windows.Forms.GroupBox();
            this.radioButtonReceive = new System.Windows.Forms.RadioButton();
            this.radioButtonSend = new System.Windows.Forms.RadioButton();
            this.groupBoxActivities = new System.Windows.Forms.GroupBox();
            this.buttonActivity = new System.Windows.Forms.Button();
            this.groupBoxGenerator.SuspendLayout();
            this.groupBoxOpentext.SuspendLayout();
            this.groupBoxciphertext.SuspendLayout();
            this.groupBoxActivities.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxGenA
            // 
            this.textBoxGenA.Location = new System.Drawing.Point(44, 32);
            this.textBoxGenA.Name = "textBoxGenA";
            this.textBoxGenA.ReadOnly = true;
            this.textBoxGenA.Size = new System.Drawing.Size(93, 20);
            this.textBoxGenA.TabIndex = 0;
            this.textBoxGenA.Text = "0";
            this.textBoxGenA.TextChanged += new System.EventHandler(this.textBoxGenA_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "A:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "X0:";
            // 
            // textBoxGenX0
            // 
            this.textBoxGenX0.Location = new System.Drawing.Point(44, 68);
            this.textBoxGenX0.Name = "textBoxGenX0";
            this.textBoxGenX0.ReadOnly = true;
            this.textBoxGenX0.Size = new System.Drawing.Size(93, 20);
            this.textBoxGenX0.TabIndex = 3;
            this.textBoxGenX0.Text = "1";
            this.textBoxGenX0.TextChanged += new System.EventHandler(this.textBoxGenX0_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "C:";
            // 
            // textBoxGenC
            // 
            this.textBoxGenC.Location = new System.Drawing.Point(44, 105);
            this.textBoxGenC.Name = "textBoxGenC";
            this.textBoxGenC.ReadOnly = true;
            this.textBoxGenC.Size = new System.Drawing.Size(93, 20);
            this.textBoxGenC.TabIndex = 5;
            this.textBoxGenC.Text = "0";
            this.textBoxGenC.TextChanged += new System.EventHandler(this.textBoxGenC_TextChanged);
            // 
            // groupBoxGenerator
            // 
            this.groupBoxGenerator.Controls.Add(this.textBoxGenX0);
            this.groupBoxGenerator.Controls.Add(this.textBoxGenC);
            this.groupBoxGenerator.Controls.Add(this.textBoxGenA);
            this.groupBoxGenerator.Controls.Add(this.label3);
            this.groupBoxGenerator.Controls.Add(this.label1);
            this.groupBoxGenerator.Controls.Add(this.label2);
            this.groupBoxGenerator.Location = new System.Drawing.Point(22, 21);
            this.groupBoxGenerator.Name = "groupBoxGenerator";
            this.groupBoxGenerator.Size = new System.Drawing.Size(167, 149);
            this.groupBoxGenerator.TabIndex = 6;
            this.groupBoxGenerator.TabStop = false;
            this.groupBoxGenerator.Text = "Генератор ПСП";
            // 
            // textBoxOpentext
            // 
            this.textBoxOpentext.Location = new System.Drawing.Point(21, 35);
            this.textBoxOpentext.Multiline = true;
            this.textBoxOpentext.Name = "textBoxOpentext";
            this.textBoxOpentext.ReadOnly = true;
            this.textBoxOpentext.Size = new System.Drawing.Size(364, 118);
            this.textBoxOpentext.TabIndex = 7;
            this.textBoxOpentext.TextChanged += new System.EventHandler(this.textBoxOpentext_TextChanged);
            // 
            // textBoxCiphertext
            // 
            this.textBoxCiphertext.Location = new System.Drawing.Point(20, 35);
            this.textBoxCiphertext.Multiline = true;
            this.textBoxCiphertext.Name = "textBoxCiphertext";
            this.textBoxCiphertext.ReadOnly = true;
            this.textBoxCiphertext.Size = new System.Drawing.Size(364, 118);
            this.textBoxCiphertext.TabIndex = 8;
            this.textBoxCiphertext.TextChanged += new System.EventHandler(this.textBoxCiphertext_TextChanged);
            // 
            // groupBoxOpentext
            // 
            this.groupBoxOpentext.Controls.Add(this.textBoxOpentext);
            this.groupBoxOpentext.Location = new System.Drawing.Point(22, 188);
            this.groupBoxOpentext.Name = "groupBoxOpentext";
            this.groupBoxOpentext.Size = new System.Drawing.Size(403, 177);
            this.groupBoxOpentext.TabIndex = 9;
            this.groupBoxOpentext.TabStop = false;
            this.groupBoxOpentext.Text = "Открытый текст:";
            // 
            // groupBoxciphertext
            // 
            this.groupBoxciphertext.Controls.Add(this.textBoxCiphertext);
            this.groupBoxciphertext.Location = new System.Drawing.Point(22, 385);
            this.groupBoxciphertext.Name = "groupBoxciphertext";
            this.groupBoxciphertext.Size = new System.Drawing.Size(403, 181);
            this.groupBoxciphertext.TabIndex = 10;
            this.groupBoxciphertext.TabStop = false;
            this.groupBoxciphertext.Text = "Зашифрованное сообшение:";
            // 
            // radioButtonReceive
            // 
            this.radioButtonReceive.AutoSize = true;
            this.radioButtonReceive.Location = new System.Drawing.Point(17, 31);
            this.radioButtonReceive.Name = "radioButtonReceive";
            this.radioButtonReceive.Size = new System.Drawing.Size(168, 17);
            this.radioButtonReceive.TabIndex = 11;
            this.radioButtonReceive.TabStop = true;
            this.radioButtonReceive.Text = "Получить приказ дяди Сэма";
            this.radioButtonReceive.UseVisualStyleBackColor = true;
            this.radioButtonReceive.CheckedChanged += new System.EventHandler(this.radioButtonReceive_CheckedChanged);
            // 
            // radioButtonSend
            // 
            this.radioButtonSend.AutoSize = true;
            this.radioButtonSend.Location = new System.Drawing.Point(17, 65);
            this.radioButtonSend.Name = "radioButtonSend";
            this.radioButtonSend.Size = new System.Drawing.Size(160, 17);
            this.radioButtonSend.TabIndex = 12;
            this.radioButtonSend.TabStop = true;
            this.radioButtonSend.Text = "Передать отчёт дяде Сэму";
            this.radioButtonSend.UseVisualStyleBackColor = true;
            this.radioButtonSend.CheckedChanged += new System.EventHandler(this.radioButtonSend_CheckedChanged);
            // 
            // groupBoxActivities
            // 
            this.groupBoxActivities.Controls.Add(this.buttonActivity);
            this.groupBoxActivities.Controls.Add(this.radioButtonReceive);
            this.groupBoxActivities.Controls.Add(this.radioButtonSend);
            this.groupBoxActivities.Location = new System.Drawing.Point(210, 21);
            this.groupBoxActivities.Name = "groupBoxActivities";
            this.groupBoxActivities.Size = new System.Drawing.Size(215, 149);
            this.groupBoxActivities.TabIndex = 13;
            this.groupBoxActivities.TabStop = false;
            this.groupBoxActivities.Text = "Действие:";
            // 
            // buttonActivity
            // 
            this.buttonActivity.Enabled = false;
            this.buttonActivity.Location = new System.Drawing.Point(64, 105);
            this.buttonActivity.Name = "buttonActivity";
            this.buttonActivity.Size = new System.Drawing.Size(75, 23);
            this.buttonActivity.TabIndex = 13;
            this.buttonActivity.Text = "Сделай!";
            this.buttonActivity.UseVisualStyleBackColor = true;
            this.buttonActivity.Click += new System.EventHandler(this.buttonActivity_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 579);
            this.Controls.Add(this.groupBoxActivities);
            this.Controls.Add(this.groupBoxciphertext);
            this.Controls.Add(this.groupBoxOpentext);
            this.Controls.Add(this.groupBoxGenerator);
            this.Name = "Form1";
            this.Text = "Тайнописька";
            this.groupBoxGenerator.ResumeLayout(false);
            this.groupBoxGenerator.PerformLayout();
            this.groupBoxOpentext.ResumeLayout(false);
            this.groupBoxOpentext.PerformLayout();
            this.groupBoxciphertext.ResumeLayout(false);
            this.groupBoxciphertext.PerformLayout();
            this.groupBoxActivities.ResumeLayout(false);
            this.groupBoxActivities.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxGenA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxGenX0;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxGenC;
        private System.Windows.Forms.GroupBox groupBoxGenerator;
        private System.Windows.Forms.TextBox textBoxOpentext;
        private System.Windows.Forms.TextBox textBoxCiphertext;
        private System.Windows.Forms.GroupBox groupBoxOpentext;
        private System.Windows.Forms.GroupBox groupBoxciphertext;
        private System.Windows.Forms.RadioButton radioButtonReceive;
        private System.Windows.Forms.RadioButton radioButtonSend;
        private System.Windows.Forms.GroupBox groupBoxActivities;
        private System.Windows.Forms.Button buttonActivity;
    }
}

