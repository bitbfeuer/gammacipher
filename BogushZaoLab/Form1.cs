﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BogushZaoLab
{
    public partial class Form1 : Form
    {

        public StreamGammaCipher StreamCipher = new StreamGammaCipher();
        
        public Form1()
        {
            InitializeComponent();
            if (StreamCipher != null)
                try
                {
                    StreamCipher.Activity = StreamGammaCipher.ActivityTypes.None;
                    StreamCipher.A = Convert.ToInt32(textBoxGenA.Text.ToString());
                    StreamCipher.X0 = Convert.ToInt32(textBoxGenX0.Text.ToString());
                    StreamCipher.C = Convert.ToInt32(textBoxGenC.Text.ToString());

                }
                catch (Exception)
                {

                }
            


        }
           
               
        private void radioButtonReceive_CheckedChanged(object sender, EventArgs e)
        {
            textBoxGenA.ReadOnly = false;
            textBoxGenX0.ReadOnly = false;
            textBoxGenC.ReadOnly = false;

            textBoxOpentext.ReadOnly = true;
            textBoxOpentext.Text = "";
            textBoxCiphertext.ReadOnly = false;

            buttonActivity.Enabled = false;

            if (StreamCipher != null)
                StreamCipher.Activity = StreamGammaCipher.ActivityTypes.Decrypt;
            

        }

        private void radioButtonSend_CheckedChanged(object sender, EventArgs e)
        {
            textBoxGenA.ReadOnly = false;
            textBoxGenX0.ReadOnly = false;
            textBoxGenC.ReadOnly = false;

            textBoxCiphertext.ReadOnly = true;
            textBoxCiphertext.Text = "";
            textBoxOpentext.ReadOnly = false;

            buttonActivity.Enabled = false;

            if (StreamCipher != null)
                StreamCipher.Activity = StreamGammaCipher.ActivityTypes.Encrypt;
        }

        private void textBoxCiphertext_TextChanged(object sender, EventArgs e)
        {
            buttonActivity.Enabled = true;

            if (StreamCipher != null)
            {
                StreamCipher.Ciphertext = textBoxCiphertext.Text;
            }


        }

        private void textBoxOpentext_TextChanged(object sender, EventArgs e)
        {
            buttonActivity.Enabled = true;

            if (StreamCipher != null)
            {
                StreamCipher.Opentext = textBoxOpentext.Text;
            }
        }

      
        private void textBoxGenA_TextChanged(object sender, EventArgs e)
        {
            if (StreamCipher != null)
                try
                {
                    StreamCipher.A = Convert.ToInt32(textBoxGenA.Text.ToString());
                }
                catch (Exception)
                { 
                
                }
        }

        private void textBoxGenX0_TextChanged(object sender, EventArgs e)
        {
            if (StreamCipher != null)
                try
                {
                    StreamCipher.X0 = Convert.ToInt32(textBoxGenX0.Text.ToString());
                }
                catch (Exception)
                {

                }
        }

        private void textBoxGenC_TextChanged(object sender, EventArgs e)
        {
            if (StreamCipher != null)
                try
                {
                    StreamCipher.C = Convert.ToInt32(textBoxGenC.Text.ToString());
                }
                catch (Exception)
                {

                }
        }

        private void buttonActivity_Click(object sender, EventArgs e)
        {
            StreamCipher.ProcessMessage();

            if (StreamCipher.Activity == StreamGammaCipher.ActivityTypes.Decrypt)
            {
                textBoxCiphertext.ReadOnly = true;

                textBoxOpentext.Text = StreamCipher.Opentext;
                textBoxOpentext.ReadOnly = false;
            }
            else if (StreamCipher.Activity == StreamGammaCipher.ActivityTypes.Encrypt)
            {
                textBoxOpentext.ReadOnly = true;

                textBoxCiphertext.Text = StreamCipher.Ciphertext;
                textBoxCiphertext.ReadOnly = false;
            }
            else
            { }
        }

    }
}
