﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//DEBUG
//using System.Windows.Forms;

namespace BogushZaoLab
{
    public class StreamGammaCipher
    {
        #region инициализация
        public StreamGammaCipher()
        {
            N = 65536;
            Activity = ActivityTypes.None;
        }
       
        public enum ActivityTypes { Decrypt, Encrypt, None };

        #endregion

        #region свойства

        private ActivityTypes activity;

        public ActivityTypes Activity
        { 
            get {return activity;}
            set { activity = value; }
        }

        private String opentext, ciphertext;

        public String Opentext
        {
            get { return opentext;}
            set { opentext = value; }
        }

        public String Ciphertext
        {
            get { return ciphertext; }
            set { ciphertext = value; }
        }

        private int a, c, x0;

        public int A
        {
            get { return a; }
            set
            {
                if ((value < 10) && (value > -10))
                    a = value;
                else a = 1;
            }
        }

        public int X0
        { 
            get { return x0; }
            set 
            {
                if ((value < 10) && (value > -10))
                    x0 = value;
                else x0 = 1;
            }
        }

        public int C
        {
            get {return c;}
            set 
            {
                if ((value < 100) && (value > -100))
                    c = value;
                else c = 0;
            } 
        }

        private int arcrazr;

        public int N         //разрядность для шифроблоков в потоке
        { get { return arcrazr; } set { arcrazr = value; } }

        private int[] keys;

        public int[] Keys
        {
            get { return keys; }
            set { keys = value; }
        }
        //*/
        
        #endregion

        

        public int ProcessMessage()
        {
            if (Activity == ActivityTypes.None)
                return 1;
            
            GeneratePSP(A, X0, C, out keys);

            String openSt = Opentext;
            String closedSt = Ciphertext;
       

            if (Activity == ActivityTypes.Encrypt)
                modulo_two(ref openSt, ref closedSt, ref keys);
            else if (Activity == ActivityTypes.Decrypt)
                modulo_two(ref closedSt, ref openSt, ref keys);

            Opentext = openSt;
            Ciphertext = closedSt;
            return 0;
        }

     

        #region служебные методы

        public void modulo_two(ref String intext, ref String restext, ref int[] key)
        { 
            int KeyLength = key.Length;

            char[] inpt = intext.ToCharArray();

            int[] buf = new int[intext.Length];

            char[] rest = new char[intext.Length];

            for (int i = 0; i < intext.Length; i++)
            {
                buf[i] = (int)inpt[i] ^ key[i];
                rest[i] = (char)buf[i];
            }

            restext = new String(rest);
          
        }

        public void GeneratePSP(int a, int x, int c, out int[] psp)
        {
            int KeyLength = 0;
            
            if (Activity == ActivityTypes.Decrypt)
                KeyLength = Ciphertext.Length;
            else if (Activity == ActivityTypes.Encrypt)
                KeyLength = Opentext.Length;
            else
            {
                psp = new int[KeyLength];
                return;
            }

            psp = new int[KeyLength];

            psp[0] = x;

            //System.Console.Write("{0} ", psp[0]);

            for (int i = 1; i < KeyLength; i++ )
            {
                psp[i] = CalculateNextX(a, ref x, c);
                //System.Console.Write("{0} ", psp[i]);
            }

            return;
        }

        public int CalculateNextX(int a, ref int x, int c)
        {
            return x = ((a * x) + c) % N;   
        }

        #endregion
    }
}
